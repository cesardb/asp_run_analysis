# ASP_run_analysis
# This projects aims at providing tools to interprete runs output of the parallel_stereo function from the Ames Stereo Pipeline (https://stereopipeline.readthedocs.io/en/latest/index.html)
# It focuses on visualisation of the execution time, the memory use, the success or failure of the function.

```
cd existing_repo
git remote add origin https://framagit.org/cesardb/asp_run_analysis.git
git branch -M main
git push -uf origin main
```
