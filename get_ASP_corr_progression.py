#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from osgeo import gdal
import datetime
import argparse

def get_ASP_corr_progression( inLog, inIm, outFolder):
    
    # Get the image size
    Im=gdal.Open(inIm)
    lxIm=Im.RasterXSize
    lyIm=Im.RasterYSize
    
    # get the processed tiles by examining the log file
    rec_p1=[]
    rec_p2=[]
    rec=[]
    
    with open(inLog) as log:
        for line in log:
            if 'log-stereo_corr' in line:
                if 'pair1' in line:
                    switch_pair_triplet="triplet"
                    if line.count('/')==3:
                        x0=float(line.split(' ')[-1].split('/')[-1].split('_')[0])
                        y0=float(line.split(' ')[-1].split('/')[-1].split('_')[1])
                        lx=float(line.split(' ')[-1].split('/')[-1].split('_')[2])
                        ly=float(line.split(' ')[-1].split('/')[-1].split('_')[3].split('-')[0])
                        rec_p1.append(mpl.patches.Rectangle((x0,y0),width=lx,height=ly,alpha=0.5,color=(0.05,1,0.05),ec='k'))
                elif 'pair2' in line:
                    if line.count('/')==3:
                        x0=float(line.split(' ')[-1].split('/')[-1].split('_')[0])
                        y0=float(line.split(' ')[-1].split('/')[-1].split('_')[1])
                        lx=float(line.split(' ')[-1].split('/')[-1].split('_')[2])
                        ly=float(line.split(' ')[-1].split('/')[-1].split('_')[3].split('-')[0])
                        rec_p2.append(mpl.patches.Rectangle((x0,y0),width=lx,height=ly,alpha=0.5,color=(0.05,1,0.05),ec='k'))
                else:
                    switch_pair_triplet="pair"
                    if not line.split(' ')[-1].split('/')[-1].split('_')[0][0].isalpha():
                        x0=float(line.split(' ')[-1].split('/')[-1].split('_')[0])
                        y0=float(line.split(' ')[-1].split('/')[-1].split('_')[1])
                        lx=float(line.split(' ')[-1].split('/')[-1].split('_')[2])
                        ly=float(line.split(' ')[-1].split('/')[-1].split('_')[3].split('-')[0])
                        rec.append(mpl.patches.Rectangle((x0,y0),width=lx,height=ly,alpha=0.5,color=(0.05,1,0.05),ec='k'))
    
    if switch_pair_triplet=="triplet":
        plt.figure()
        ax=plt.subplot(1,2,1)
        rec_Im=mpl.patches.Rectangle((0,0),width=lxIm,height=lyIm,color=(0.2,0.2,0.2),zorder=0)
        ax.add_patch(rec_Im)                
        colec=PatchCollection(rec_p1,alpha=0.5,color=(0.05,1,0.05),edgecolor='k',zorder=100)
        ax.add_collection(colec)                    

        plt.axis('tight')
        plt.axis('equal')
        plt.box(False)
        plt.title('Pair 1')

        ax=plt.subplot(1,2,2)
        rec_Im=mpl.patches.Rectangle((0,0),width=lxIm,height=lyIm,color=(0.2,0.2,0.2),zorder=0)
        ax.add_patch(rec_Im)
        colec=PatchCollection(rec_p2,alpha=0.5,color=(0.05,1,0.05),edgecolor='k',zorder=100)
        ax.add_collection(colec) 

        plt.axis('tight')
        plt.axis('equal')
        plt.box(False)
        plt.title('Pair 2')
        time_str=str(datetime.datetime.now().year)+str(datetime.datetime.now().month)+str(datetime.datetime.now().day)+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)
        plt.savefig(outFolder+'/ASP_corr_progress_'+time_str+'.png')
    elif switch_pair_triplet=="pair":
        plt.figure()
        ax=plt.subplot(1,1,1)
        rec_Im=mpl.patches.Rectangle((0,0),width=lxIm,height=lyIm,color=(0.2,0.2,0.2),zorder=0)
        ax.add_patch(rec_Im)                
        colec=PatchCollection(rec,alpha=0.5,color=(0.05,1,0.05),edgecolor='k',zorder=100)
        ax.add_collection(colec)                    

        plt.axis('tight')
        plt.axis('equal')
        plt.box(False)
        plt.title('Pair 1')
        time_str=str(datetime.datetime.now().year)+str(datetime.datetime.now().month)+str(datetime.datetime.now().day)+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)
        plt.savefig(outFolder+'/ASP_corr_progress_'+time_str+'.png')
        
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Shows how many tiles have been processed by the stereo correlation step.")
    parser.add_argument("-inLog",dest = "inLog")
    parser.add_argument("-inIm",dest = "inIm")
    parser.add_argument("-outFolder",dest = "outFolder")
    args = parser.parse_args()

    get_ASP_corr_progression( args.inLog, args.inIm, args.outFolder)
