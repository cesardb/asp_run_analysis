#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from osgeo import gdal
import datetime as datetime
import numpy as np
import glob as glob
import argparse

def extract_tile_exec_time(logFolder, ASPstep, outFolder, suffix):
        print(ASPstep)
        time_exec_unique=[]
        rec=[]
        tile_folder_list=glob.glob(logFolder+"/*/")
        for tile_folder in tile_folder_list:
            if "pair" not in tile_folder:
                print(tile_folder)
                inLoglist=glob.glob(tile_folder+"/*"+ASPstep+"*.txt")
                if len(inLoglist)>0:
                    inLog=inLoglist[0]
                    x0=int(inLog.split('/')[-1].split('_')[0])
                    y0=int(inLog.split('/')[-1].split('_')[1])
                    lx=int(inLog.split('/')[-1].split('_')[2])
                    ly=int(inLog.split('/')[-1].split('_')[3].split('-')[0])
                    with open(inLog) as log:
                            for line in log:
                                if 'started' in line:
                                    dstart=datetime.datetime.strptime(line[-21:-2],"%Y-%m-%d %H:%M:%S")
                                elif 'FINISHED' in line:
                                    dend=datetime.datetime.strptime(line[2:22],"%Y-%b-%d %H:%M:%S")
                            dexec=dend-dstart              
                    #time_exec_arr[y0:y0+ly,x0:x0+lx]=dexec.seconds
                    time_exec_unique.append(dexec.seconds)
                    rec.append(mpl.patches.Rectangle((x0,y0),width=lx,height=ly,ec='k'))                      
                        
                                
        ## plot the map of the execution time per tile
        fig, axs = plt.subplots(1, 2, figsize=(6,6))
        colec=PatchCollection(rec,edgecolor='k',cmap=mpl.cm.YlOrRd,zorder=100,clim=(0,np.max(time_exec_unique)))
        colec.set_array(np.array(time_exec_unique))
        axs[0].add_collection(colec)  

        axins = inset_axes(axs[0],width="100%",height="5%",loc='lower center',borderpad=-5)
        fig.colorbar(colec, cax=axins, orientation="horizontal")

        axs[0].axis('equal')
        
        
        ## plot the histogram of the execution time per tile
        axs[1].hist(np.array(time_exec_unique).flatten())
        asp = np.diff(axs[1].get_xlim())[0] / np.diff(axs[1].get_ylim())[0]
        axs[1].set_aspect(asp)
        axs[1].grid(True)
        axs[1].set_axisbelow(True)
        axs[1].set_title('time exec '+ASPstep+' (s)')
        
        fig.tight_layout() #pad=3.0
        plt.savefig(outFolder+'/time_exec_ASP_'+ASPstep+'_'+suffix+'.png')
        plt.close()        
        
def get_ASP_tile_exec_time(inFolder, outFolder):
    
    if len(glob.glob(inFolder+"/*pair1/"))>0:
        # tri-stereo
        extract_tile_exec_time(inFolder, 'log-stereo_tri', outFolder, "")

        ttFolder=glob.glob(inFolder+"/*pair1/")
        extract_tile_exec_time(ttFolder, 'log-stereo_corr', outFolder, "pair1")
        extract_tile_exec_time(ttFolder, 'log-stereo_blend', outFolder, "pair1")
        
        ttFolder=glob.glob(inFolder+"/*pair2/")
        extract_tile_exec_time(ttFolder, 'log-stereo_corr', outFolder, "pair2")
        extract_tile_exec_time(ttFolder, 'log-stereo_blend', outFolder, "pair2")
        
    else:
        # bi-stereo
        extract_tile_exec_time(inFolder, 'log-stereo_tri', outFolder, "")
        extract_tile_exec_time(inFolder, 'log-stereo_corr', outFolder, "pair1")
        extract_tile_exec_time(inFolder, 'log-stereo_blend', outFolder, "pair1")


                        
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Shows the execution time for each tile the stereo correlation, blending and triangulation step.")
    parser.add_argument("-inFolder",dest = "inFolder")
    parser.add_argument("-outFolder",dest = "outFolder")
    args = parser.parse_args()

    get_ASP_tile_exec_time( args.inFolder, args.outFolder)
